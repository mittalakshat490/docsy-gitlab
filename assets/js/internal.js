$( document ).ready(() => {
const urlParams = new URLSearchParams(window.location.search);
const redirectFrom = urlParams.get('redirected-from');
if (redirectFrom === oldHandbookUrl) {
    if(document.getElementsByClassName('td-navbar-cover').length > 0) {
        const section = document.createElement("section")
        const outterDiv =  document.createElement("div");
        const containerDiv = document.createElement("div");
        const mbDiv = document.createElement("div");
        const colDiv = document.createElement("div");
        const message = document.createElement("h2");
        section.classList = "row td-box td-box--danger text-white position-relative td-box--height-auto";
        outterDiv.classList = "col-12";
        containerDiv.classList = "container text-center td-arrow-down";
        mbDiv.classList = "h4 mb-0"
        colDiv.classList = "col"
        colDiv.innerHTML = '<h2 class="text-center"><i class="fa-regular fa-triangle-exclamation"></i> Please update your bookmark or link <i class="fa-regular fa-triangle-exclamation"></i></h2><p class="text-center">You were redirected here from <strong>internal-handbook.gitlab.io</strong> which is now <strong>deprecated</strong></p>';
        mbDiv.appendChild(colDiv);
        containerDiv.appendChild(mbDiv);
        outterDiv.appendChild(containerDiv);
        section.appendChild(outterDiv);
        const main = document.getElementsByTagName('main')[0];
        main.insertBefore(section, document.getElementsByTagName('section')[1]);
    } else {
        const warn = document.createElement("div");
        warn.classList = "card mb-4";
        const warnHeader = document.createElement("div");
        warnHeader.classList = "card-header bg-danger text-white";
        warnHeader.innerHTML = "<strong>Warning:</strong>  Please update your bookmark or link.  You were redirected here from <strong>internal-handbook.gitlab.io</strong> which is now <strong>deprecated</strong>";
        warn.appendChild(warnHeader);
        const main = document.getElementsByTagName('main')[0];
        main.insertBefore(warn, main.firstChild);
    }
    renderNotification({title: "Please update your bookmark or link", messageHTML: "<p class=\"h5\">You were redirected here from <strong>internal-handbook.gitlab.io</strong> which is <strong>now deprecated</strong></p>",type: "danger", icon: "fa-solid fa-diamond-turn-right", autohide: true},null)
}});
