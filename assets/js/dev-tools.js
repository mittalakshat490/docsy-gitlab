$( document ).ready(() => {
  function htmlLink() {
    return location.pathname.substr(0, location.pathname.length-1) + ".html"
  }

  function getOldLink() {
    if(location.pathname.includes("support")) {
      if(location.pathname.includes("workflows"))
        return "https://about.gitlab.com" + htmlLink();
    }
    return "https://about.gitlab.com" + location.pathname;
  }

  function enableDevTools() {
    // Set up Dev tools heading and div
    sideLinks = document.getElementsByClassName("td-page-meta ml-2 pb-3 pt-2 mb-0")[0];
    if(sideLinks == null)
      return;
    devHeading = document.createElement("h5")
    devHeading.setAttribute("id", "devHeading");
    devHeading.classList.add("td-sidebar-link", "tree-root");
    devHeading.innerHTML = "Dev Tools"
    devToolLinks = document.createElement("div");
    devToolLinks.setAttribute("id", "devToolLinks");
    devToolLinks.classList.add("td-page-meta", "ml-2", "pb-3", "pt-2", "mb-0");

    viewOnOldHandbook = document.createElement("a");
    viewOnOldHandbook.href = getOldLink();
    viewOnOldHandbook.target = "_blank";
    viewOnOldHandbook.classList.add("td-page-meta");
    viewOnOldHandbook.innerHTML = "<i class=\"fa-solid fa-book\"></i> View on old Handbook";

    copyRedirectLink = document.createElement("a");
    copyRedirectLink.href = "#"
    copyRedirectLink.classList.add("td-page-meta");
    copyRedirectLink.innerHTML = "<i class=\"fa-regular fa-copy\"></i> Copy redirect";
    copyRedirectLink.setAttribute("id", "copyRedirectLink");
    copyRedirectLink.onclick = function() {
      redirectText = htmlLink() + " " + location.pathname;
      navigator.clipboard.writeText(redirectText);
      renderNotification({
        "id":  crypto.randomUUID(),
        "type": "success",
        "icon": "fa-regular fa-copy",
        "title": "Redirect copied to clipboard",
        "messageHTML": "<p class=\"h5\">Redirect has been copied to clipboard.  Your redirect code is:</p><hr><code>" + redirectText + "</code>",
        "posted": new Date(),
        "autohide": true
      }, false);
    };

    viewOnOldHandbookAlt = document.createElement("a");
    if("https://about.gitlab.com" + htmlLink() != getOldLink()) {
      viewOnOldHandbookAlt.href = "https://about.gitlab.com" + htmlLink();
      viewOnOldHandbookAlt.target = "_blank";
      viewOnOldHandbookAlt.classList.add("td-page-meta");
      viewOnOldHandbookAlt.innerHTML = "<i class=\"fa-solid fa-book-bookmark\"></i> View on old Handbook (Alt)";
    }

    devToolLinks.appendChild(devHeading);
    devToolLinks.appendChild(viewOnOldHandbook);
    devToolLinks.appendChild(viewOnOldHandbookAlt);
    devToolLinks.appendChild(copyRedirectLink);
    sideLinks.after(devToolLinks);
    if(devPreferences.notification)
      renderNotification({
        "id":  crypto.randomUUID(),
        "type": "warning",
        "icon": "fa-solid fa-screwdriver-wrench",
        "title": "Dev Tools Enabled",
        "messageHTML": "<p class=\"h5\">Developer tools have been enabled.</p>",
        "posted": new Date(),
        "autohide": true
      }, false);
  }

  function disableDevTools() {
    document.getElementById("devToolLinks").remove();
    renderNotification({
      "id":  crypto.randomUUID(),
      "type": "danger",
      "icon": "fa-solid fa-screwdriver-wrench",
      "title": "Dev Tools Disabled",
      "messageHTML": "<p class=\"h5\">Developer tools have been disabled.</p>",
      "posted": new Date(),
      "autohide": true
    }, false);
  }

  devPreferences = getCookie("devtool-preferences");

  if(devPreferences.length == 0)
    setCookie("devtool-preferences", JSON.stringify({enabled: false, notification: true}), 365);
  else
    devPreferences = JSON.parse(devPreferences);

  if(devPreferences.enabled)
    enableDevTools()

  if(document.getElementById("enableDevToolsSwitch")) {
    devToolsEnableSwitch = document.getElementById("enableDevToolsSwitch")
    devToolsEnableSwitch.disabled = false;
    devToolsEnableSwitch.checked = devPreferences.enabled;
    devToolsEnableSwitch.onchange = (e) => {
      setCookie("devtool-preferences", JSON.stringify({enabled: e.target.checked, notification: devPreferences.notification}), 365);
      if(devToolsEnableSwitch.checked)
        enableDevTools();
      else
        disableDevTools();
    }
  }
  if(document.getElementById("showDevNotificationSwitch")) {
    devToolsNotificationSwitch = document.getElementById("showDevNotificationSwitch")
    devToolsNotificationSwitch.disabled = false;
    devToolsNotificationSwitch.checked = devPreferences.notification;
    devToolsNotificationSwitch.onchange = (e) => {
      setCookie("devtool-preferences", JSON.stringify({enabled: devPreferences.enabled, notification: e.target.checked}), 365);
    }
  }

});
